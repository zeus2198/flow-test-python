from . import node_abstract

class TimeDelayNode(node_abstract.Node):
  def __init__(self, data, config, nodeId):
    super().__init__(data, config, nodeId)
    # now we do additional checks for this node
    # we need delay attr for this node
    if "delay" not in config:
      raise Exception("Delay attr missing", nodeId)
    

  def perform(self):
    # time delay node doesnt have anything to perform
    pass

  def resolveNextNodes(self):
    # simply return next node with delay defined in config
    return list(map(lambda x: (x, self.config["delay"]), self.config["nextNodes"]))
        
        
