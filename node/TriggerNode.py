from . import node_abstract

class TriggerNode(node_abstract.Node):
  def perform(self):
    # doest really do anything
    pass

  def resolveNextNodes(self):
    # simply return next nodes with delay 0
    return list(map(lambda x: (x, 0), self.config["nextNodes"]))
        
        
